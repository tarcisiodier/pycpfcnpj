Code Documentation
===================================

pycnpfj.calculation
--------------------
.. automodule:: calculation
   :members: 

pycnpfj.cpf
--------------------
.. automodule:: cpf
   :members:

pycnpfj.cnpj
--------------------
.. automodule:: cnpj
   :members:

pycnpfj.cnpfj
--------------------
.. automodule:: cnpfj
   :members:                  